﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;

namespace ConnectFour
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        #region Private Members
        private bool PlayerOneTurn;

        private bool GameEnded;
        private bool Winner;

        private string PlayerOne = "Black";
        private string PlayerTwo = "Blue";

        private int col0BtnCount = -1;
        private int col1BtnCount = -1;
        private int col2BtnCount = -1;
        private int col3BtnCount = -1;
        private int col4BtnCount = -1;
        private int col5BtnCount = -1;

        public Border panel;
        public int iCol;
        public int iRow;

        private int row;
        private int col;
        #endregion

        public MainWindow()
        {        
            InitializeComponent();
            createPanels();
        }

        public void createPanels() {
            PlayerOneTurn = true;
            int iRow = -1;
            foreach (RowDefinition row in Container.RowDefinitions) 
            {
                iRow++;
                int iCol = -1;
                foreach (ColumnDefinition col in Container.ColumnDefinitions)
                {
                    iCol++;
                    Border panel = new Border();
                    Grid.SetRow(panel, iRow);
                    Grid.SetColumn(panel, iCol);
                    var panelName = "col" + iCol + "row" + iRow;
                    Container.RegisterName( panelName, panel);
                    panel.Name = "col" + iCol + "row" + iRow;
                    panel.Background = Brushes.Red;
                    panel.CornerRadius = new CornerRadius(50);
                    panel.MouseEnter += Panel_MouseEnter;
                    panel.MouseLeave += Panel_MouseLeave;
                    panel.Margin = new Thickness(2);
                    panel.Background = new SolidColorBrush(Color.FromArgb(255, 255, 255, 255));
                    //panel.MouseDown += Panel_MouseDown;
                    Container.Children.Add(panel);
                }
            }
        }
        
        private void Panel_MouseLeave(object sender, MouseEventArgs e)
        {
            Border panel = sender as Border;
            panel.BorderThickness = new Thickness(0);
        }

        private void Panel_MouseEnter(object sender, MouseEventArgs e)
        {
            Border panel = sender as Border;
            panel.BorderThickness = new Thickness(1);
            panel.BorderBrush = new SolidColorBrush(Color.FromArgb(200, 20, 20, 20));
            int col = Grid.GetColumn(panel);
            int row = Grid.GetRow(panel);
            string sCol = " col=" + col;
            string sRow = " row=" + row;

        }

        //private void Panel_MouseDown(object sender, MouseButtonEventArgs e)
        //{
        //    //if (GameEnded)
        //    //{
        //    //    return;
        //    //}

        //    panel = (Border)sender;
        //    iCol = Grid.GetColumn(panel);
        //    iRow = Grid.GetRow(panel);
        //    //CheckWin();

        //    //CheckForWinner(iCol, iRow);
  
        //}

        public Border getPanel (int col, int row) 
        {
            Border panel = (Border)FindName($"col{col}row{row}");
            return panel;    
        }

        public bool CheckWin(SolidColorBrush x) 
        {

            var counter = -1;
            Border[] colRowArray = new Border[42];
            for (int i = 0; i < 6; i++)
            {
                for (int j = 0; j < 7; j++)
                {
                    counter++;
                    colRowArray[counter] = getPanel(i, j);
              
                }

            }           

            //Check rows 0 1 2 3 all match
            if (colRowArray[0].Background == x
                && colRowArray[1].Background == x
                && colRowArray[2].Background == x 
                && colRowArray[3].Background == x)
            {
                return true;
            }

            //Checks if rows 1 2 3 4 all match 
            if (colRowArray[1].Background == x
                && colRowArray[2].Background == x
                && colRowArray[3].Background == x
                && colRowArray[4].Background == x)
            {
                return true;
            }

            //Checks if rows 2 3 4 5 all match
            if (colRowArray[2].Background == x
                && colRowArray[3].Background == x
                && colRowArray[4].Background == x
                && colRowArray[5].Background == x)
            {
                return true;
            }
            /********* End Col 0 Row Check **************/


            //Check if rows 6 7 8 9 all match
            if (colRowArray[6].Background == x 
                && colRowArray[7].Background == x 
                && colRowArray[8].Background == x
                && colRowArray[9].Background == x)
            {
                return true;
            }

            //Check if rows 7 8 9 10 all match
            if (colRowArray[7].Background == x 
                && colRowArray[8].Background == x
                && colRowArray[9].Background == x
                && colRowArray[10].Background == x)
            {
                return true;
            }

            //Check if rows 8 9 10 11 all match
            if (colRowArray[8].Background == x 
                && colRowArray[9].Background == x 
                && colRowArray[10].Background == x 
                && colRowArray[11].Background == x)
            {
                return true;
            }

            /********* End Col 1 Check **************/

            //Check if rows 12 13 14 15 all match
            if (colRowArray[12].Background == x
                && colRowArray[13].Background == x
                && colRowArray[14].Background == x
                && colRowArray[15].Background == x)
            {
                return true;
            }

            //Check if rows 13 14 15 16 all match
            if (colRowArray[13].Background == x
                && colRowArray[14].Background == x
                && colRowArray[15].Background == x
                && colRowArray[16].Background == x)
            {
                return true;
            }

            //Check if rows 14 15 16 17 all match
            if (colRowArray[14].Background == x
                && colRowArray[15].Background == x
                && colRowArray[16].Background == x
                && colRowArray[17].Background == x)
            {
                return true;
            }


            /********* End Col 2 Check **************/

            //Check if rows 18 19 20 21 all match
            if (colRowArray[18].Background == x
                && colRowArray[19].Background == x
                && colRowArray[20].Background == x
                && colRowArray[21].Background == x)
            {
                return true;
            }

            //Check if rows 19 20 21 22 all match
            if (colRowArray[19].Background == x
                && colRowArray[20].Background == x
                && colRowArray[21].Background == x
                && colRowArray[22].Background == x)
            {
                return true;
            }

            //Check if rows 20 21 22 23 all match
            if (colRowArray[20].Background == x
                && colRowArray[21].Background == x
                && colRowArray[22].Background == x
                && colRowArray[23].Background == x)
            {
                return true;
            }

            /********* End Col 3 Check **************/

            //Check if rows 25 26 27 all match
            if (colRowArray[24].Background == x
                && colRowArray[25].Background == x
                && colRowArray[26].Background == x
                && colRowArray[27].Background == x)
            {
                return true;
            }

            //Check if rows 25 26 27 28 all match
            if (colRowArray[25].Background == x
                && colRowArray[26].Background == x
                && colRowArray[27].Background == x
                && colRowArray[28].Background == x)
            {
                return true;
            }

            //Check if rows 26 27 28 29 all match
            if (colRowArray[26].Background == x
                && colRowArray[27].Background == x
                && colRowArray[28].Background == x
                && colRowArray[29].Background == x)
            {
                return true;
            }

            /********* End Col 4 Check **************/

            //Check if rows 30 31 32 33 all match
            if (colRowArray[30].Background == x
                && colRowArray[31].Background == x
                && colRowArray[32].Background == x
                && colRowArray[33].Background == x)
            {
                return true;
            }

            //Check if rows 31 32 33 34 all match
            if (colRowArray[31].Background == x
                && colRowArray[32].Background == x
                && colRowArray[33].Background == x
                && colRowArray[34].Background == x)
            {
                return true;
            }

            //Check if rows 32 33 34 35 all match
            if (colRowArray[32].Background == x
                && colRowArray[33].Background == x
                && colRowArray[34].Background == x
                && colRowArray[35].Background == x)
            {
                return true;
            }

            /********* End Col 5 Check **************/

            //Check if rows 36 37 38 39 all match
            if (colRowArray[36].Background == x
                && colRowArray[37].Background == x
                && colRowArray[38].Background == x
                && colRowArray[39].Background == x)
            {
                return true;
            }

            //Check if rows 37 38 39 40 all match
            if (colRowArray[37].Background == x
                && colRowArray[38].Background == x
                && colRowArray[39].Background == x
                && colRowArray[40].Background == x)
            {
                return true;
            }

            //Check if rows 38 39 40 41 all match
            if (colRowArray[38].Background == x
                && colRowArray[39].Background == x
                && colRowArray[40].Background == x
                && colRowArray[41].Background == x)
            {
                return true;
            }

            /********* End Col 6 Check **************/

            //Check if rows 0 6 12 18 all match
            if (colRowArray[0].Background == x
                && colRowArray[6].Background == x
                && colRowArray[12].Background == x
                && colRowArray[18].Background == x)
            {
                return true;
            }

            //Check if rows 6 12 18 24 all match
            if (colRowArray[6].Background == x
                && colRowArray[12].Background == x
                && colRowArray[18].Background == x
                && colRowArray[24].Background == x)
            {
                return true;
            }

            //Check if rows 12 18 24 30 all match
            if (colRowArray[12].Background == x
                && colRowArray[18].Background == x
                && colRowArray[24].Background == x
                && colRowArray[30].Background == x)
            {
                return true;
            }

            //Check if rows 18 24 30 36 all match
            if (colRowArray[18].Background == x
                && colRowArray[24].Background == x
                && colRowArray[30].Background == x
                && colRowArray[36].Background == x)
            {
                return true;
            }
            /********* End Col 6 Check **************/

            //Check if columns 0 6 12 18 all match
            if (colRowArray[0].Background == x
                && colRowArray[6].Background == x
                && colRowArray[12].Background == x
                && colRowArray[18].Background == x)
            {
                return true;
            }

            //Check if columns 6 12 18 24 all match
            if (colRowArray[6].Background == x
                && colRowArray[12].Background == x
                && colRowArray[18].Background == x
                && colRowArray[24].Background == x)
            {
                return true;
            }

            //Check if columns 12 18 24 30 all match
            if (colRowArray[12].Background == x
                && colRowArray[18].Background == x
                && colRowArray[24].Background == x
                && colRowArray[30].Background == x)
            {
                return true;
            }

            //Check if columns 18 24 30 36 all match
            if (colRowArray[18].Background == x
                && colRowArray[24].Background == x
                && colRowArray[30].Background == x
                && colRowArray[36].Background == x)
            {
                return true;
            }
            /********* End Row 0 Check **************/

            //Check if columns 1 7 13 19 all match
            if (colRowArray[1].Background == x
                && colRowArray[7].Background == x
                && colRowArray[13].Background == x
                && colRowArray[19].Background == x)
            {
                return true;
            }

            //Check if columns 7 13 19 25 all match
            if (colRowArray[7].Background == x
                && colRowArray[13].Background == x
                && colRowArray[19].Background == x
                && colRowArray[25].Background == x)
            {
                return true;
            }

            //Check if columns 12 18 24 30 all match
            if (colRowArray[12].Background == x
                && colRowArray[18].Background == x
                && colRowArray[24].Background == x
                && colRowArray[30].Background == x)
            {
                return true;
            }

            //Check if columns 18 24 30 36 all match
            if (colRowArray[18].Background == x
                && colRowArray[24].Background == x
                && colRowArray[30].Background == x
                && colRowArray[36].Background == x)
            {
                return true;
            }
            /********* End Row 1 Check **************/

            //Check if columns 2 9 15 21 all match
            if (colRowArray[2].Background == x
                && colRowArray[8].Background == x
                && colRowArray[14].Background == x
                && colRowArray[20].Background == x)
            {
                return true;
            }

            //Check if columns 8 14 20 26 all match
            if (colRowArray[8].Background == x
                && colRowArray[14].Background == x
                && colRowArray[20].Background == x
                && colRowArray[26].Background == x)
            {
                return true;
            }

            //Check if columns 14 20 26 32 all match
            if (colRowArray[14].Background == x
                && colRowArray[20].Background == x
                && colRowArray[26].Background == x
                && colRowArray[32].Background == x)
            {
                return true;
            }

            //Check if columns 20 26 32 38 all match
            if (colRowArray[20].Background == x
                && colRowArray[26].Background == x
                && colRowArray[32].Background == x
                && colRowArray[38].Background == x)
            {
                return true;
            }
            /********* End Row 2 Check **************/

            //Check if columns 3 9 15 21 all match
            if (colRowArray[3].Background == x
                && colRowArray[9].Background == x
                && colRowArray[15].Background == x
                && colRowArray[21].Background == x)
            {
                return true;
            }

            //Check if columns 9 15 21 27 all match
            if (colRowArray[9].Background == x
                && colRowArray[15].Background == x
                && colRowArray[21].Background == x
                && colRowArray[27].Background == x)
            {
                return true;
            }

            //Check if columns 15 21 27 33 all match
            if (colRowArray[15].Background == x
                && colRowArray[21].Background == x
                && colRowArray[27].Background == x
                && colRowArray[33].Background == x)
            {
                return true;
            }

            //Check if columns 21 27 33 39 all match
            if (colRowArray[21].Background == x
                && colRowArray[27].Background == x
                && colRowArray[33].Background == x
                && colRowArray[39].Background == x)
            {
                return true;
            }
            /********* End Row 3 Check **************/

            //Check if columns 4 10 16 22 all match
            if (colRowArray[4].Background == x
                && colRowArray[10].Background == x
                && colRowArray[16].Background == x
                && colRowArray[22].Background == x)
            {
                return true;
            }

            //Check if columns 10 16 22 28 all match
            if (colRowArray[10].Background == x
                && colRowArray[16].Background == x
                && colRowArray[22].Background == x
                && colRowArray[28].Background == x)
            {
                return true;
            }

            //Check if columns 16 22 28 34 all match
            if (colRowArray[16].Background == x
                && colRowArray[22].Background == x
                && colRowArray[28].Background == x
                && colRowArray[34].Background == x)
            {
                return true;
            }

            //Check if columns 22 28 34 40 all match
            if (colRowArray[22].Background == x
                && colRowArray[28].Background == x
                && colRowArray[34].Background == x
                && colRowArray[40].Background == x)
            {
                return true;
            }
            /********* End Row 4 Check **************/

            //Check if columns 5 11 17 23 all match
            if (colRowArray[5].Background == x
                && colRowArray[11].Background == x
                && colRowArray[17].Background == x
                && colRowArray[23].Background == x)
            {
                return true;
            }

            //Check if columns 11 17 23 29 all match
            if (colRowArray[11].Background == x
                && colRowArray[17].Background == x
                && colRowArray[23].Background == x
                && colRowArray[29].Background == x)
            {
                return true;
            }

            //Check if columns 17 23 29 35 all match
            if (colRowArray[17].Background == x
                && colRowArray[23].Background == x
                && colRowArray[29].Background == x
                && colRowArray[35].Background == x)
            {
                return true;
            }

            //Check if columns 23 29 35 41 all match
            if (colRowArray[23].Background == x
                && colRowArray[29].Background == x
                && colRowArray[35].Background == x
                && colRowArray[41].Background == x)
            {
                return true;
            }

            /********* End Row 5 Check ***********/

            //Check if cells 18 13 8 3 all match
            if (colRowArray[18].Background == x
                && colRowArray[13].Background == x
                && colRowArray[8].Background == x
                && colRowArray[3].Background == x)
            {
                return true;
            }

            //Check if cells 24 19 14 9 all match
            if (colRowArray[24].Background == x
                && colRowArray[19].Background == x
                && colRowArray[14].Background == x
                && colRowArray[9].Background == x)
            {
                return true;
            }

            //Check if cells 19 14 9 4 all match
            if (colRowArray[19].Background == x
                && colRowArray[14].Background == x
                && colRowArray[9].Background == x
                && colRowArray[4].Background == x)
            {
                return true;
            }


            //Check if cells 30 25 20 15 all match
            if (colRowArray[30].Background == x
                && colRowArray[25].Background == x
                && colRowArray[20].Background == x
                && colRowArray[15].Background == x)
            {
                return true;
            }

            //Check if cells 25 20 15 10 all match
            if (colRowArray[25].Background == x
                && colRowArray[20].Background == x
                && colRowArray[15].Background == x
                && colRowArray[10].Background == x)
            {
                return true;
            }

            //Check if cells 20 15 10 5 all match
            if (colRowArray[20].Background == x
                && colRowArray[15].Background == x
                && colRowArray[10].Background == x
                && colRowArray[5].Background == x)
            {
                return true;
            }

            //Check if cells 36 31 26 21 all match
            if (colRowArray[36].Background == x
                && colRowArray[31].Background == x
                && colRowArray[26].Background == x
                && colRowArray[21].Background == x)
            {
                return true;
            }

            //Check if cells 31 26 21 16 all match
            if (colRowArray[31].Background == x
                && colRowArray[26].Background == x
                && colRowArray[21].Background == x 
                && colRowArray[16].Background == x)
            {
                return true;
            }

            //Check if cells 31 26 21 11 all match
            if (colRowArray[26].Background == x
                && colRowArray[21].Background == x
                && colRowArray[16].Background == x
                && colRowArray[11].Background == x)
            {
                return true;
            }

            //Check if cells 37 32 27 22 all match
            if (colRowArray[37].Background == x
                && colRowArray[32].Background == x
                && colRowArray[27].Background == x
                && colRowArray[22].Background == x)
            {
                return true;
            }

            //Check if cells 32 27 22 17 all match
            if (colRowArray[32].Background == x
                && colRowArray[27].Background == x
                && colRowArray[22].Background == x
                && colRowArray[17].Background == x)
            {
                return true;
            }

            //Check if cells 38 33 28 23 all match
            if (colRowArray[38].Background == x
                && colRowArray[33].Background == x
                && colRowArray[28].Background == x
                && colRowArray[23].Background == x)
            {
                return true;
            }

            //Check if cells 2 9 16 23 all match
            if (colRowArray[2].Background == x
                && colRowArray[9].Background == x
                && colRowArray[16].Background == x
                && colRowArray[23].Background == x)
            {
                return true;
            }

            //Check if cells 1 8 15 22 all match
            if (colRowArray[1].Background == x
                && colRowArray[8].Background == x
                && colRowArray[15].Background == x
                && colRowArray[22].Background == x)
            {
                return true;
            }

            //Check if cells 8 15 22 29 all match
            if (colRowArray[8].Background == x
                && colRowArray[15].Background == x
                && colRowArray[22].Background == x
                && colRowArray[29].Background == x)
            {
                return true;
            }

            //Check if cells 0 7 14 21 all match
            if (colRowArray[0].Background == x
                && colRowArray[7].Background == x
                && colRowArray[14].Background == x
                && colRowArray[21].Background == x)
            {
                return true;
            }

            //Check if cells 7 14 21 28 all match
            if (colRowArray[7].Background == x
                && colRowArray[14].Background == x
                && colRowArray[21].Background == x
                && colRowArray[28].Background == x)
            {
                return true;
            }

            //Check if cells 14 21 28 35 all match
            if (colRowArray[14].Background == x
                && colRowArray[21].Background == x
                && colRowArray[28].Background == x
                && colRowArray[35].Background == x)
            {
                return true;
            }

            //Check if cells 6 13 20 27 all match
            if (colRowArray[6].Background == x
                && colRowArray[13].Background == x
                && colRowArray[20].Background == x
                && colRowArray[27].Background == x)
            {
                return true;
            }

            //Check if cells 13 20 27 34 all match
            if (colRowArray[13].Background == x
                && colRowArray[20].Background == x
                && colRowArray[27].Background == x
                && colRowArray[34].Background == x)
            {
                return true;
            }

            //Check if cells 20 27 34 41 all match
            if (colRowArray[20].Background == x
                && colRowArray[27].Background == x
                && colRowArray[34].Background == x
                && colRowArray[41].Background == x)
            {
                return true;
            }

            //Check if cells 12 19 26 33 all match
            if (colRowArray[12].Background == x
                && colRowArray[19].Background == x
                && colRowArray[26].Background == x
                && colRowArray[33].Background == x)
            {
                return true;
            }

            //Check if cells 19 26 33 40 all match
            if (colRowArray[19].Background == x
                && colRowArray[26].Background == x
                && colRowArray[33].Background == x
                && colRowArray[40].Background == x)
            {
                return true;
            }

            //Check if cells 18 25 32 39 all match
            if (colRowArray[18].Background == x
                && colRowArray[25].Background == x
                && colRowArray[32].Background == x
                && colRowArray[39].Background == x)
            {
                return true;
            }

            return false;
        }


        #region Cell Color Changes      
        /// <summary>
        /// All of the functions below will change the color of the cell depending on the player
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void col0Btn_Click(object sender, RoutedEventArgs e)
        {
            //Change the cell color depending on how many times the button was clicked
            col0BtnCount++;
            //Changes the color of cell depending on the amount of clicks
            if (col0BtnCount == 0)
            {
                Border panel = (Border)this.FindName("col0row5");
                if (PlayerOneTurn)
                {
                    panel.Background = Brushes.Black;
                    PlayerOneTurn = false;
                    Winner = CheckWin(Brushes.Black);
                    if (Winner)
                    {
                        GameEnded = true;
                        winnerLabel.Content = "Black is the winner.";
                    }
                    playerLabel.Content = "Blue turn!";
                }
                else
                {
                    panel.Background = Brushes.Blue;
                    PlayerOneTurn = true;
                    Winner = CheckWin(Brushes.Blue);
                    if (Winner)
                    {
                        GameEnded = true;
                        winnerLabel.Content = "Blue is the winner.";
                    }
                    playerLabel.Content = "Black turn!";
                }

            }

            if (col0BtnCount == 1)
            {
                Border panel = (Border)this.FindName("col0row4");
                if (PlayerOneTurn)
                {
                    panel.Background = Brushes.Black;
                    PlayerOneTurn = false;
                    Winner = CheckWin(Brushes.Black);
                    if (Winner)
                    {
                        GameEnded = true;
                        winnerLabel.Content = "Black is the winner.";
                    }
                    playerLabel.Content = "Blue turn!";
                }
                else
                {
                    panel.Background = Brushes.Blue;
                    PlayerOneTurn = true;
                    Winner = CheckWin(Brushes.Blue);
                    if (Winner)
                    {
                        GameEnded = true;
                        winnerLabel.Content = "Blue is the winner.";
                    }
                    playerLabel.Content = "Black turn!";
                }
            }

            if (col0BtnCount == 2)
            {
                Border panel = (Border)this.FindName("col0row3");
                if (PlayerOneTurn)
                {
                    panel.Background = Brushes.Black;
                    PlayerOneTurn = false;
                    Winner = CheckWin(Brushes.Black);
                    if (Winner)
                    {
                        GameEnded = true;
                        winnerLabel.Content = "Black is the winner.";
                    }
                    playerLabel.Content = "Blue turn!";
                }
                else
                {
                    panel.Background = Brushes.Blue;
                    PlayerOneTurn = true;
                    Winner = CheckWin(Brushes.Blue);
                    if (Winner)
                    {
                        GameEnded = true;
                        winnerLabel.Content = "Blue is the winner.";
                    }
                    playerLabel.Content = "Black turn!";
                }
            }

            if (col0BtnCount == 3)
            {
                Border panel = (Border)this.FindName("col0row2");
                if (PlayerOneTurn)
                {
                    panel.Background = Brushes.Black;
                    PlayerOneTurn = false;
                    Winner = CheckWin(Brushes.Black);
                    if (Winner)
                    {
                        GameEnded = true;
                        winnerLabel.Content = "Black is the winner.";
                    }
                    playerLabel.Content = "Blue turn!";
                }
                else
                {
                    panel.Background = Brushes.Blue;
                    PlayerOneTurn = true;
                    Winner = CheckWin(Brushes.Blue);
                    if (Winner)
                    {
                        GameEnded = true;
                        winnerLabel.Content = "Blue is the winner.";
                    }
                    playerLabel.Content = "Black turn!";
                }
            }

            if (col0BtnCount == 4)
            {
                Border panel = (Border)this.FindName("col0row1");
                if (PlayerOneTurn)
                {
                    panel.Background = Brushes.Black;
                    PlayerOneTurn = false;
                    Winner = CheckWin(Brushes.Black);
                    if (Winner)
                    {
                        GameEnded = true;
                        winnerLabel.Content = "Black is the winner.";
                    }
                    playerLabel.Content = "Blue turn!";
                }
                else
                {
                    panel.Background = Brushes.Blue;
                    PlayerOneTurn = true;
                    Winner = CheckWin(Brushes.Blue);
                    if (Winner)
                    {
                        GameEnded = true;
                        winnerLabel.Content = "Blue is the winner.";
                    }
                    playerLabel.Content = "Black turn!";
                }
            }

            if (col0BtnCount == 5)
            {
                Border panel = (Border)this.FindName("col0row0");
                if (PlayerOneTurn)
                {
                    panel.Background = Brushes.Black;
                    PlayerOneTurn = false;
                    Winner = CheckWin(Brushes.Black);
                    if (Winner)
                    {
                        GameEnded = true;
                        winnerLabel.Content = "Black is the winner.";
                    }
                    playerLabel.Content = "Blue turn!";
                }
                else
                {
                    panel.Background = Brushes.Blue;
                    PlayerOneTurn = true;
                    Winner = CheckWin(Brushes.Blue);
                    if (Winner)
                    {
                        GameEnded = true;
                        winnerLabel.Content = "Blue is the winner.";
                    }
                    playerLabel.Content = "Black turn!";
                }
            }

        }
        private void col1Btn_Click(object sender, RoutedEventArgs e)
        {
            //Change the cell color depending on how many times the button was clicked
            col1BtnCount++;
            //Changes the color of cell depending on the amount of clicks
            if (col1BtnCount == 0)
            {
                Border panel = (Border)this.FindName("col1row5");
                if (PlayerOneTurn)
                {
                    panel.Background = Brushes.Black;
                    PlayerOneTurn = false;
                    Winner = CheckWin(Brushes.Black);
                    if (Winner)
                    {
                        GameEnded = true;
                        winnerLabel.Content = "Black is the winner.";
                    }
                    playerLabel.Content = "Blue turn!";
                }
                else
                {
                    panel.Background = Brushes.Blue;
                    PlayerOneTurn = true;
                    Winner = CheckWin(Brushes.Blue);
                    if (Winner)
                    {
                        GameEnded = true;
                        winnerLabel.Content = "Blue is the winner.";
                    }
                    playerLabel.Content = "Black turn!";
                }

            }

            if (col1BtnCount == 1)
            {
                Border panel = (Border)this.FindName("col1row4");
                if (PlayerOneTurn)
                {
                    panel.Background = Brushes.Black;
                    PlayerOneTurn = false;
                    Winner = CheckWin(Brushes.Black);
                    if (Winner)
                    {
                        GameEnded = true;
                        winnerLabel.Content = "Black is the winner.";
                    }
                    playerLabel.Content = "Blue turn!";
                }
                else
                {
                    panel.Background = Brushes.Blue;
                    PlayerOneTurn = true;
                    Winner = CheckWin(Brushes.Blue);
                    if (Winner)
                    {
                        GameEnded = true;
                        winnerLabel.Content = "Blue is the winner.";
                    }
                    playerLabel.Content = "Black turn!";
                }
            }

            if (col1BtnCount == 2)
            {
                Border panel = (Border)this.FindName("col1row3");
                if (PlayerOneTurn)
                {
                    panel.Background = Brushes.Black;
                    PlayerOneTurn = false;
                    Winner = CheckWin(Brushes.Black);
                    if (Winner)
                    {
                        GameEnded = true;
                        winnerLabel.Content = "Black is the winner.";
                    }
                    playerLabel.Content = "Blue turn!";
                }
                else
                {
                    panel.Background = Brushes.Blue;
                    PlayerOneTurn = true;
                    Winner = CheckWin(Brushes.Blue);
                    if (Winner)
                    {
                        GameEnded = true;
                        winnerLabel.Content = "Blue is the winner.";
                    }
                    playerLabel.Content = "Black turn!";
                }
            }

            if (col1BtnCount == 3)
            {
                Border panel = (Border)this.FindName("col1row2");
                if (PlayerOneTurn)
                {
                    panel.Background = Brushes.Black;
                    PlayerOneTurn = false;
                    Winner = CheckWin(Brushes.Black);
                    if (Winner)
                    {
                        GameEnded = true;
                        winnerLabel.Content = "Black is the winner.";
                    }
                    playerLabel.Content = "Blue turn!";
                }
                else
                {
                    panel.Background = Brushes.Blue;
                    PlayerOneTurn = true;
                    Winner = CheckWin(Brushes.Blue);
                    if (Winner)
                    {
                        GameEnded = true;
                        winnerLabel.Content = "Blue is the winner.";
                    }
                    playerLabel.Content = "Black turn!";
                }
            }

            if (col1BtnCount == 4)
            {
                Border panel = (Border)this.FindName("col1row1");
                if (PlayerOneTurn)
                {
                    panel.Background = Brushes.Black;
                    PlayerOneTurn = false;
                    Winner = CheckWin(Brushes.Black);
                    if (Winner)
                    {
                        GameEnded = true;
                        winnerLabel.Content = "Black is the winner.";
                    }
                    playerLabel.Content = "Blue turn!";
                }
                else
                {
                    panel.Background = Brushes.Blue;
                    PlayerOneTurn = true;
                    Winner = CheckWin(Brushes.Blue);
                    if (Winner)
                    {
                        GameEnded = true;
                        winnerLabel.Content = "Blue is the winner.";
                    }
                    playerLabel.Content = "Black turn!";
                }
            }

            if (col1BtnCount == 5)
            {
                Border panel = (Border)this.FindName("col1row0");
                if (PlayerOneTurn)
                {
                    panel.Background = Brushes.Black;
                    PlayerOneTurn = false;
                    Winner = CheckWin(Brushes.Black);
                    if (Winner)
                    {
                        GameEnded = true;
                        winnerLabel.Content = "Black is the winner.";
                    }
                    playerLabel.Content = "Blue turn!";
                }
                else
                {
                    panel.Background = Brushes.Blue;
                    PlayerOneTurn = true;
                    Winner = CheckWin(Brushes.Blue);
                    if (Winner)
                    {
                        GameEnded = true;
                        winnerLabel.Content = "Blue is the winner.";
                    }
                    playerLabel.Content = "Black turn!";
                }
            }
        }
        private void col2Btn_Click(object sender, RoutedEventArgs e)
        {
            //Change the cell color depending on how many times the button was clicked
            col2BtnCount++;
            //Changes the color of cell depending on the amount of clicks
            if (col2BtnCount == 0)
            {
                Border panel = (Border)this.FindName("col2row5");
                if (PlayerOneTurn)
                {
                    panel.Background = Brushes.Black;
                    PlayerOneTurn = false;
                    Winner = CheckWin(Brushes.Black);
                    if (Winner)
                    {
                        GameEnded = true;
                        winnerLabel.Content = "Black is the winner.";
                    }
                    playerLabel.Content = "Blue turn!";
                }
                else
                {
                    panel.Background = Brushes.Blue;
                    PlayerOneTurn = true;
                    Winner = CheckWin(Brushes.Blue);
                    if (Winner)
                    {
                        GameEnded = true;
                        winnerLabel.Content = "Blue is the winner.";
                    }
                    playerLabel.Content = "Black turn!";
                }
            }

            if (col2BtnCount == 1)
            {
                Border panel = (Border)this.FindName("col2row4");
                if (PlayerOneTurn)
                {
                    panel.Background = Brushes.Black;
                    PlayerOneTurn = false;
                    Winner = CheckWin(Brushes.Black);
                    if (Winner)
                    {
                        GameEnded = true;
                        winnerLabel.Content = "Black is the winner.";
                    }
                    playerLabel.Content = "Blue turn!";
                }
                else
                {
                    panel.Background = Brushes.Blue;
                    PlayerOneTurn = true;
                    Winner = CheckWin(Brushes.Blue);
                    if (Winner)
                    {
                        GameEnded = true;
                        winnerLabel.Content = "Blue is the winner.";
                    }
                    playerLabel.Content = "Black turn!";
                }
            }

            if (col2BtnCount == 2)
            {
                Border panel = (Border)this.FindName("col2row3");
                if (PlayerOneTurn)
                {
                    panel.Background = Brushes.Black;
                    PlayerOneTurn = false;
                    Winner = CheckWin(Brushes.Black);
                    if (Winner)
                    {
                        GameEnded = true;
                        winnerLabel.Content = "Black is the winner.";
                    }
                    playerLabel.Content = "Blue turn!";
                }
                else
                {
                    panel.Background = Brushes.Blue;
                    PlayerOneTurn = true;
                    Winner = CheckWin(Brushes.Blue);
                    if (Winner)
                    {
                        GameEnded = true;
                        winnerLabel.Content = "Blue is the winner.";
                    }
                    playerLabel.Content = "Black turn!";
                }
            }

            if (col2BtnCount == 3)
            {
                Border panel = (Border)this.FindName("col2row2");
                if (PlayerOneTurn)
                {
                    panel.Background = Brushes.Black;
                    PlayerOneTurn = false;
                    Winner = CheckWin(Brushes.Black);
                    if (Winner)
                    {
                        GameEnded = true;
                        winnerLabel.Content = "Black is the winner.";
                    }
                    playerLabel.Content = "Blue turn!";
                }
                else
                {
                    panel.Background = Brushes.Blue;
                    PlayerOneTurn = true;
                    Winner = CheckWin(Brushes.Blue);
                    if (Winner)
                    {
                        GameEnded = true;
                        winnerLabel.Content = "Blue is the winner.";
                    }
                    playerLabel.Content = "Black turn!";
                }
            }

            if (col2BtnCount == 4)
            {
                Border panel = (Border)this.FindName("col2row1");
                if (PlayerOneTurn)
                {
                    panel.Background = Brushes.Black;
                    PlayerOneTurn = false;
                    Winner = CheckWin(Brushes.Black);
                    if (Winner)
                    {
                        GameEnded = true;
                        winnerLabel.Content = "Black is the winner.";
                    }
                    playerLabel.Content = "Blue turn!";
                }
                else
                {
                    panel.Background = Brushes.Blue;
                    PlayerOneTurn = true;
                    Winner = CheckWin(Brushes.Blue);
                    if (Winner)
                    {
                        GameEnded = true;
                        winnerLabel.Content = "Blue is the winner.";
                    }
                    playerLabel.Content = "Black turn!";
                }
            }

            if (col2BtnCount == 5)
            {
                Border panel = (Border)this.FindName("col2row0");
                if (PlayerOneTurn)
                {
                    panel.Background = Brushes.Black;
                    PlayerOneTurn = false;
                    Winner = CheckWin(Brushes.Black);
                    if (Winner)
                    {
                        GameEnded = true;
                        winnerLabel.Content = "Black is the winner.";
                    }
                    playerLabel.Content = "Blue turn!";
                }
                else
                {
                    panel.Background = Brushes.Blue;
                    PlayerOneTurn = true;
                    Winner = CheckWin(Brushes.Blue);
                    if (Winner)
                    {
                        GameEnded = true;
                        winnerLabel.Content = "Blue is the winner.";
                    }
                    playerLabel.Content = "Black turn!";
                }
            }
        }
        private void col3Btn_Click(object sender, RoutedEventArgs e)
        {
            //Change the cell color depending on how many times the button was clicked
            col3BtnCount++;
            //Changes the color of cell depending on the amount of clicks
            if (col3BtnCount == 0)
            {
                Border panel = (Border)this.FindName("col3row5");
                if (PlayerOneTurn)
                {
                    panel.Background = Brushes.Black;
                    PlayerOneTurn = false;
                    Winner = CheckWin(Brushes.Black);
                    if (Winner)
                    {
                        GameEnded = true;
                        winnerLabel.Content = "Black is the winner.";
                    }
                    playerLabel.Content = "Blue turn!";
                }
                else
                {
                    panel.Background = Brushes.Blue;
                    PlayerOneTurn = true;
                    Winner = CheckWin(Brushes.Blue);
                    if (Winner)
                    {
                        GameEnded = true;
                        winnerLabel.Content = "Blue is the winner.";
                    }
                    playerLabel.Content = "Black turn!";
                }
            }

            if (col3BtnCount == 1)
            {
                Border panel = (Border)this.FindName("col3row4");
                if (PlayerOneTurn)
                {
                    panel.Background = Brushes.Black;
                    PlayerOneTurn = false;
                    Winner = CheckWin(Brushes.Black);
                    if (Winner)
                    {
                        GameEnded = true;
                        winnerLabel.Content = "Black is the winner.";
                    }
                    playerLabel.Content = "Blue turn!";
                }
                else
                {
                    panel.Background = Brushes.Blue;
                    PlayerOneTurn = true;
                    Winner = CheckWin(Brushes.Blue);
                    if (Winner)
                    {
                        GameEnded = true;
                        winnerLabel.Content = "Blue is the winner.";
                    }
                    playerLabel.Content = "Black turn!";
                }
            }

            if (col3BtnCount == 2)
            {
                Border panel = (Border)this.FindName("col3row3");
                if (PlayerOneTurn)
                {
                    panel.Background = Brushes.Black;
                    PlayerOneTurn = false;
                    Winner = CheckWin(Brushes.Black);
                    if (Winner)
                    {
                        GameEnded = true;
                        winnerLabel.Content = "Black is the winner.";
                    }
                    playerLabel.Content = "Blue turn!";
                }
                else
                {
                    panel.Background = Brushes.Blue;
                    PlayerOneTurn = true;
                    Winner = CheckWin(Brushes.Blue);
                    if (Winner)
                    {
                        GameEnded = true;
                        winnerLabel.Content = "Blue is the winner.";
                    }
                    playerLabel.Content = "Black turn!";
                }
            }

            if (col3BtnCount == 3)
            {
                Border panel = (Border)this.FindName("col3row2");
                if (PlayerOneTurn)
                {
                    panel.Background = Brushes.Black;
                    PlayerOneTurn = false;
                    Winner = CheckWin(Brushes.Black);
                    if (Winner)
                    {
                        GameEnded = true;
                        winnerLabel.Content = "Black is the winner.";
                    }
                    playerLabel.Content = "Blue turn!";
                }
                else
                {
                    panel.Background = Brushes.Blue;
                    PlayerOneTurn = true;
                    Winner = CheckWin(Brushes.Blue);
                    if (Winner)
                    {
                        GameEnded = true;
                        winnerLabel.Content = "Blue is the winner.";
                    }
                    playerLabel.Content = "Black turn!";
                }
            }

            if (col3BtnCount == 4)
            {
                Border panel = (Border)this.FindName("col3row1");
                if (PlayerOneTurn)
                {
                    panel.Background = Brushes.Black;
                    PlayerOneTurn = false;
                    Winner = CheckWin(Brushes.Black);
                    if (Winner)
                    {
                        GameEnded = true;
                        winnerLabel.Content = "Black is the winner.";
                    }
                    playerLabel.Content = "Blue turn!";
                }
                else
                {
                    panel.Background = Brushes.Blue;
                    PlayerOneTurn = true;
                    Winner = CheckWin(Brushes.Blue);
                    if (Winner)
                    {
                        GameEnded = true;
                        winnerLabel.Content = "Blue is the winner.";
                    }
                    playerLabel.Content = "Black turn!";
                }
            }

            if (col3BtnCount == 5)
            {
                Border panel = (Border)this.FindName("col3row0");
                if (PlayerOneTurn)
                {
                    panel.Background = Brushes.Black;
                    PlayerOneTurn = false;
                    Winner = CheckWin(Brushes.Black);
                    if (Winner)
                    {
                        GameEnded = true;
                        winnerLabel.Content = "Black is the winner.";
                    }
                    playerLabel.Content = "Blue turn!";
                }
                else
                {
                    panel.Background = Brushes.Blue;
                    PlayerOneTurn = true;
                    Winner = CheckWin(Brushes.Blue);
                    if (Winner)
                    {
                        GameEnded = true;
                        winnerLabel.Content = "Blue is the winner.";
                    }
                    playerLabel.Content = "Blue turn!";
                }
            }
        }
        private void col4Btn_Click(object sender, RoutedEventArgs e)
        {
            //Change the cell color depending on how many times the button was clicked
            col4BtnCount++;
            //Changes the color of cell depending on the amount of clicks
            if (col4BtnCount == 0)
            {
                Border panel = (Border)this.FindName("col4row5");
                if (PlayerOneTurn)
                {
                    panel.Background = Brushes.Black;
                    PlayerOneTurn = false;
                    Winner = CheckWin(Brushes.Black);
                    if (Winner)
                    {
                        GameEnded = true;
                        winnerLabel.Content = "Black is the winner.";
                    }
                    playerLabel.Content = "Blue turn!";
                }
                else
                {
                    panel.Background = Brushes.Blue;
                    PlayerOneTurn = true;
                    Winner = CheckWin(Brushes.Blue);
                    if (Winner)
                    {
                        GameEnded = true;
                        winnerLabel.Content = "Blue is the winner.";
                    }
                    playerLabel.Content = "Black turn!";
                }
            }

            if (col4BtnCount == 1)
            {
                Border panel = (Border)this.FindName("col4row4");
                if (PlayerOneTurn)
                {
                    panel.Background = Brushes.Black;
                    PlayerOneTurn = false;
                    Winner = CheckWin(Brushes.Black);
                    if (Winner)
                    {
                        GameEnded = true;
                        winnerLabel.Content = "Black is the winner.";
                    }
                    playerLabel.Content = "Blue turn!";
                }
                else
                {
                    panel.Background = Brushes.Blue;
                    PlayerOneTurn = true;
                    Winner = CheckWin(Brushes.Blue);
                    if (Winner)
                    {
                        GameEnded = true;
                        winnerLabel.Content = "Blue is the winner.";
                    }
                    playerLabel.Content = "Black turn!";
                }
            }

            if (col4BtnCount == 2)
            {
                Border panel = (Border)this.FindName("col4row3");
                if (PlayerOneTurn)
                {
                    panel.Background = Brushes.Black;
                    PlayerOneTurn = false;
                    Winner = CheckWin(Brushes.Black);
                    if (Winner)
                    {
                        GameEnded = true;
                        winnerLabel.Content = "Black is the winner.";
                    }
                    playerLabel.Content = "Blue turn!";
                }
                else
                {
                    panel.Background = Brushes.Blue;
                    PlayerOneTurn = true;
                    Winner = CheckWin(Brushes.Blue);
                    if (Winner)
                    {
                        GameEnded = true;
                        winnerLabel.Content = "Blue is the winner.";
                    }
                    playerLabel.Content = "Black turn!";
                }
            }

            if (col4BtnCount == 3)
            {
                Border panel = (Border)this.FindName("col4row2");
                if (PlayerOneTurn)
                {
                    panel.Background = Brushes.Black;
                    PlayerOneTurn = false;
                    Winner = CheckWin(Brushes.Black);
                    if (Winner)
                    {
                        GameEnded = true;
                        winnerLabel.Content = "Black is the winner.";
                    }
                    playerLabel.Content = "Blue turn!";
                }
                else
                {
                    panel.Background = Brushes.Blue;
                    PlayerOneTurn = true;
                    Winner = CheckWin(Brushes.Blue);
                    if (Winner)
                    {
                        GameEnded = true;
                        winnerLabel.Content = "Blue is the winner.";
                    }
                    playerLabel.Content = "Black turn!";
                }
            }

            if (col4BtnCount == 4)
            {
                Border panel = (Border)this.FindName("col4row1");
                if (PlayerOneTurn)
                {
                    panel.Background = Brushes.Black;
                    PlayerOneTurn = false;
                    Winner = CheckWin(Brushes.Black);
                    if (Winner)
                    {
                        GameEnded = true;
                        winnerLabel.Content = "Black is the winner.";
                    }
                    playerLabel.Content = "Blue turn!";
                }
                else
                {
                    panel.Background = Brushes.Blue;
                    PlayerOneTurn = true;
                    Winner = CheckWin(Brushes.Blue);
                    if (Winner)
                    {
                        GameEnded = true;
                        winnerLabel.Content = "Blue is the winner.";
                    }
                    playerLabel.Content = "Black turn!";
                }
            }

            if (col4BtnCount == 5)
            {
                Border panel = (Border)this.FindName("col4row0");
                if (PlayerOneTurn)
                {
                    panel.Background = Brushes.Black;
                    PlayerOneTurn = false;
                    Winner = CheckWin(Brushes.Black);
                    if (Winner)
                    {
                        GameEnded = true;
                        winnerLabel.Content = "Black is the winner.";
                    }
                    playerLabel.Content = "Blue turn!";
                }
                else
                {
                    panel.Background = Brushes.Blue;
                    PlayerOneTurn = true;
                    Winner = CheckWin(Brushes.Blue);
                    if (Winner)
                    {
                        GameEnded = true;
                        winnerLabel.Content = "Blue is the winner.";
                    }
                    playerLabel.Content = "Black turn!";
                }
            }
        }
        private void col5Btn_Click(object sender, RoutedEventArgs e)
        {
            //Change the cell color depending on how many times the button was clicked
            col5BtnCount++;
            //Changes the color of cell depending on the amount of clicks

            if (col5BtnCount == 0)
            {
                Border panel = (Border)this.FindName("col5row5");
                if (PlayerOneTurn)
                {
                    panel.Background = Brushes.Black;
                    PlayerOneTurn = false;
                    Winner = CheckWin(Brushes.Black);
                    if (Winner)
                    {
                        GameEnded = true;
                        winnerLabel.Content = "Black is the winner.";
                    }
                    playerLabel.Content = "Blue turn!";
                }
                else
                {
                    panel.Background = Brushes.Blue;
                    PlayerOneTurn = true;
                    Winner = CheckWin(Brushes.Blue);
                    if (Winner)
                    {
                        GameEnded = true;
                        winnerLabel.Content = "Blue is the winner.";
                    }
                    playerLabel.Content = "Black turn!";
                }
            }

            if (col5BtnCount == 1)
            {
                Border panel = (Border)this.FindName("col5row4");
                if (PlayerOneTurn)
                {
                    panel.Background = Brushes.Black;
                    PlayerOneTurn = false;
                    Winner = CheckWin(Brushes.Black);
                    if (Winner)
                    {
                        GameEnded = true;
                        winnerLabel.Content = "Black is the winner.";
                    }
                    playerLabel.Content = "Blue turn!";
                }
                else
                {
                    panel.Background = Brushes.Blue;
                    PlayerOneTurn = true;
                    Winner = CheckWin(Brushes.Blue);
                    if (Winner)
                    {
                        GameEnded = true;
                        winnerLabel.Content = "Blue is the winner.";
                    }
                    playerLabel.Content = "Black turn!";
                }
            }

            if (col5BtnCount == 2)
            {
                Border panel = (Border)this.FindName("col5row3");
                if (PlayerOneTurn)
                {
                    panel.Background = Brushes.Black;
                    PlayerOneTurn = false;
                    Winner = CheckWin(Brushes.Black);
                    if (Winner)
                    {
                        GameEnded = true;
                        winnerLabel.Content = "Black is the winner.";
                    }
                    playerLabel.Content = "Blue turn!";
                }
                else
                {
                    panel.Background = Brushes.Blue;
                    PlayerOneTurn = true;
                    Winner = CheckWin(Brushes.Blue);
                    if (Winner)
                    {
                        GameEnded = true;
                        winnerLabel.Content = "Blue is the winner.";
                    }
                    playerLabel.Content = "Black turn!";
                }
            }

            if (col5BtnCount == 3)
            {
                Border panel = (Border)this.FindName("col5row2");
                if (PlayerOneTurn)
                {
                    panel.Background = Brushes.Black;
                    PlayerOneTurn = false;
                    Winner = CheckWin(Brushes.Black);
                    if (Winner)
                    {
                        GameEnded = true;
                        winnerLabel.Content = "Black is the winner.";
                    }
                    playerLabel.Content = "Blue turn!";
                }
                else
                {
                    panel.Background = Brushes.Blue;
                    PlayerOneTurn = true;
                    Winner = CheckWin(Brushes.Blue);
                    if (Winner)
                    {
                        GameEnded = true;
                        winnerLabel.Content = "Blue is the winner.";
                    }
                    playerLabel.Content = "Black turn!";
                }
            }

            if (col5BtnCount == 4)
            {
                Border panel = (Border)this.FindName("col5row1");
                if (PlayerOneTurn)
                {
                    panel.Background = Brushes.Black;
                    PlayerOneTurn = false;
                    Winner = CheckWin(Brushes.Black);
                    if (Winner)
                    {
                        GameEnded = true;
                        winnerLabel.Content = "Black is the winner.";
                    }
                    playerLabel.Content = "Blue turn!";
                }
                else
                {
                    panel.Background = Brushes.Blue;
                    PlayerOneTurn = true;
                    Winner = CheckWin(Brushes.Blue);
                    if (Winner)
                    {
                        GameEnded = true;
                        winnerLabel.Content = "Blue is the winner.";
                    }
                    playerLabel.Content = "Black turn!";
                }
            }

            if (col5BtnCount == 5)
            {
                Border panel = (Border)this.FindName("col5row0");
                if (PlayerOneTurn)
                {
                    panel.Background = Brushes.Black;
                    PlayerOneTurn = false;
                    Winner = CheckWin(Brushes.Black);
                    if (Winner)
                    {
                        GameEnded = true;
                        winnerLabel.Content = "Black is the winner.";
                    }
                    playerLabel.Content = "Blue turn!";
                }
                else
                {
                    panel.Background = Brushes.Blue;
                    PlayerOneTurn = true;
                    Winner = CheckWin(Brushes.Blue);
                    if (Winner)
                    {
                        GameEnded = true;
                        winnerLabel.Content = "Blue is the winner.";
                    }
                    playerLabel.Content = "Black turn!";
                }
            }
        }

        #endregion



    }
}
