﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConnectFour
{
    public class Direction
    {
        public int i { get; set; }
        public int j { get; set; }
    }
}
